drop table FILM;

create table FILM(
    idFilm int,
    titre varchar(30),
    prenomRea varchar(30),
    nomRea varchar(30),
    annee int,
    nomGenre varchar(30),
    scenario varchar(400),
    img varchar(300),
    primary key (idFilm),
    constraint key1 unique (titre) 
);
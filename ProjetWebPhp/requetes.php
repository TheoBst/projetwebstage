<?php
function connect_bd(){
    try{
        $connexion = new PDO('sqlite:./sql/film.sqlt');
    }
    catch(PDOException $e){
        printf("Échec connexion : %s\n", $e->getMessage());
        exit();
    }
return $connexion;
}

function film_recent($connexion){
    $sql = "SELECT *, max(annee) FROM FILM";
    $stmt = $connexion -> prepare($sql);
    $stmt -> execute();
    if (!$stmt) echo "Problème d'accès aux films.";
    else{
        foreach ($stmt as $row)
        echo "<img src='" .$row['img']."'height=600 width=350/></br></br>Film : ".$row['titre']."</br></br>Réalisateur : ".$row['prenomRea']." ".$row['nomRea']."</br></br>Date de sortie : ".$row['annee']."</br></br>Genre : ".$row['nomGenre']."</br></br>Scenario : ".$row['scenario']."</br></br>";
    }
}
/*function film_genre($connexion){
$genre = $connexion -> query('SELECT nomGenre from FILM');
        foreach($genre as $option){
            echo "<option value=genre-".$option['nomGenre'].">".$option['nomGenre']."</option><br>";
        }
    }
*/
function verrif_film($connexion,$titre,$nom,$prenom,$genre,$scenario,$url,$annee){
    $query = $connexion -> query('SELECT max(idFilm) as lastId FROM FILM');
    foreach ($query as $lastId) {
}   try{
    $req = $connexion->prepare("INSERT INTO FILM (idFilm, titre, prenomRea, nomRea, annee, nomGenre, scenario, img) VALUES (:id, :titre, :prenomRea, :nomRea, :annee, :nomGenre, :scenario, :img)");
    $req->bindValue(":id", $lastId['lastId']+1);
    $req->bindValue(":titre", $titre);
    $req->bindValue(":prenomRea", $prenom );
    $req->bindValue(":nomRea", $nom );
    $req->bindValue(":annee", $annee);
    $req->bindValue(":nomGenre", $genre);
    $req->bindValue(":scenario", $scenario);
    $req->bindValue(":img", $url);
    $req->execute();
    }catch(PDOException $ex){
        echo "Ce film existe deja!!!";
    }}


    function genre_film($connexion,$data){
    $genre = $connexion -> query('SELECT nomGenre from FILM');
    foreach($genre as $optionGenre){
        echo "<option value='genre-".$optionGenre['nomGenre']."'".(($data['nomGenre']==$optionGenre['nomGenre'])?" selected='selected'":"''").">".$optionGenre['nomGenre']."</option><br>";
    }
    }
    function prenom_Realisateur($connexion,$data){
    $prenomRealisateur = $connexion -> query('SELECT prenomRea from FILM');
    foreach($prenomRealisateur as $optionRea){
        echo "<option value='rea-".$optionRea['prenomRea']."'".(($data['prenomRea']==$optionRea['prenomRea'])?" selected='selected'":"''").">".$optionRea['prenomRea']."</option><br>";
    }}

    function nom_Realisateur($connexion,$data){
        $nomRealisateur = $connexion -> query('SELECT nomRea from FILM');
    foreach($nomRealisateur as $optionRea){
        echo "<option value='rea-".$optionRea['nomRea']."'".(($data['nomRea']==$optionRea['nomRea'])?" selected='selected'":"''").">".$optionRea['nomRea']."</option><br>";
    }

    }

    function creer_genre($connexion){
        $genre = $connexion -> query('SELECT distinct nomGenre FROM FILM');
        foreach($genre as $option)
        {
            echo "<option value=Genre?".$option['nomGenre'].">".$option['nomGenre']."</option><br>";
        }
    }
    
    function tri($connexion,$choice){
        if ($choice[0]=="titre"){
            $testdb = $connexion -> query('SELECT * from FILM order by '.$choice[0].' '.$choice[1]); 
        }
        else if ($choice[0]=="nomRea"){
            $testdb = $connexion -> query('SELECT * from FILM order by '.$choice[0].' '.$choice[1]);
        }
        else if ($choice[0]=="annee"){
            $testdb = $connexion -> query('SELECT * from FILM order by '.$choice[0].' '.$choice[1]);      
        }
        else if ($choice[0]=="Genre"){
            $testdb = $connexion -> query('SELECT * from FILM where "nomGenre" = "'.$choice[1].'"');
        }
        return $testdb;
    }
    
    function rechercher($connexion,$recherche){
        $allusers = $connexion->query('SELECT * FROM FILM WHERE titre LIKE"%'.$recherche.'%" ORDER BY titre');
        return $allusers;
    }
    function liste_films($connexion){
        $sql = "SELECT * FROM FILM";
        $stmt = $connexion -> prepare($sql);
        $stmt -> execute();
        if (!$stmt) echo "Problème d'accès aux films.";
        else{
            foreach ($stmt as $row)
            echo "<img src='" .$row['img']."'height=600 width=350/></br></br>Film : ".$row['titre']."</br></br>Réalisateur : ".$row['prenomRea']." ".$row['nomRea']."</br></br>Date de sortie : ".$row['annee']."</br></br>Genre : ".$row['nomGenre']."</br></br>Scenario : ".$row['scenario']."</br></br>
            <a href='Gerer.php?filmId=".$row['idFilm']."' class='gererFilm'>GERER</a>
            </br></br>";
        }
    }

    function modifier_supprimer($connexion){
        if ($_POST){
            echo "<div class='content'>";
            if (!isset($_POST['s']) and !isset($_POST['searchList']) and isset($_POST['action'])){
                $gererChoice = explode('-', $_POST['action']);
                if($gererChoice[0]=='supprimer'){
                    $supprimer = $connexion -> prepare('DELETE FROM FILM WHERE idFilm='.$gererChoice[1]);
                    $supprimer -> execute();
                    echo "Suppression effectuée";
                }
                else if($gererChoice[0]=='modifier'){
                    /*$genreChoice = explode('-', $_POST['selectGenre']);
                    $reaChoice = explode('-', $_POST['selectNomRea']);
                    $rea1Choice = explode('-', $_POST['selectPrenomRea']);*/
                    $modifQuery = "UPDATE FILM SET titre='".$_POST['filmTitre']."',annee=".$_POST['filmAnnee'].",nomGenre='".$_POST['nomGenre']."',nomRea='".$_POST['nomRea']."',prenomRea='".$_POST['prenomRea']."',scenario='".$_POST['filmScenario']."', img='".$_POST['filmImg']."' WHERE idFilm=".$gererChoice[1];
                    $modifier = $connexion -> prepare($modifQuery);
                    $modifier -> execute();
                    echo "Modification effectuée";
                }
            }

    }
}

?>

<head>
    <link href="css/RechercherFilm.css" rel="stylesheet" type="text/css"/>
</head>
<title>
Bienvenue sur Cinéland !
</title>
<?php

require_once('header.php');
require('requetes.php');
$connexion = connect_bd();
//require_once('footer.php');

echo 
    "
    <html>
    <link href='css/RechercherFilm.css' rel='stylesheet' type='text/css'>
    <form action='RechercherFilms.php' method='POST'>
    <div class='Rechercher'>
    <input class='navbarre' type='search' name='s' placeholder='Rechercher un film' autocomplete ='off'>
    <select class='navliste' name='searchList'>
        <option selected disabled>Critère</option>
        <optgroup label='Titre'>
            <option value='titre?asc'>Ordre alphabétique</option>
            <option value='titre?desc'>Ordre inverse</option>
        <optgroup label='Réalisateur'>
            <option value='nomRea?asc'>Ordre alphabétique</option>
            <option value='nomRea?desc'>Ordre inverse</option>
        <optgroup label='Année'>
            <option value='annee?desc'>Plus récent</option>
            <option value='annee?asc'>Plus ancien</option>
        </optgroup>
        <optgroup label='Genre'>
        
    ";

    creer_genre($connexion);

echo
    "
    </optgroup>
    </select> 
    </div>
    <input type='submit' name='envoyer' class='valider' value='Valider'>
    </form>

<div>
    <h1>Liste des films</h1>
</div>
</html>
    ";
modifier_supprimer($connexion);
if ($_POST){
    if(isset($_POST['s']) and $_POST['s']!=""){
        
        $recherche = $_POST['s'];
        $allusers = rechercher($connexion,$recherche);
        foreach ($allusers as $data){
            echo
            "
                <h1>".$data['titre']."</h1>
                    <img src='".$data['img']."'height=600 width=350/>
                    <div class='txtInfo'>
                        <p>".$data['annee']."</p>
                        <p>".$data['nomGenre']."</p>
                    </div>
                </div>
                <p class='filmTxt'><b>Réalisateur :</b> ".$data['prenomRea'].' '.$data['nomRea']."</p>
                <p class='filmTxt'><b>Synopsis : </b> <i>".$data['scenario']."</i></p>
                <a href='Gerer.php?filmId=".$data['idFilm']."' class='gererFilm'>GERER</a>
            </div>
            ";
        }
        echo 
        "
        </div>
            ";
        }
    //}
        else if (isset($_POST['searchList'])){
            $choice = explode('?', $_POST['searchList']);
            $testdb = tri($connexion,$choice);
            foreach ($testdb as $data){
                echo
                "
                    <h1>".$data['titre']."</h1>
                        <img src='".$data['img']."'height=600 width=350/>
                        <div class='txtInfo'>
                            <p>".$data['annee']."</p>
                            <p>".$data['nomGenre']."</p>
                            
                        </div>
                    </div>
                    <p class='filmTxt'><b>Réalisateur :</b> ".$data['prenomRea'].' '.$data['nomRea']."</p>
                    <p class='filmTxt'><b>Synopsis : </b> <i>".$data['scenario']."</i></p>
                    <a href='Gerer.php?filmId=".$data['idFilm']."' class='gererFilm'>GERER</a>
                </div>
                ";
            }
            
        }
        else{
            liste_films($connexion);
            }
    } 

else{
    liste_films($connexion);

} 
    

    
?>



